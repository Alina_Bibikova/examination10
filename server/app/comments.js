const express = require('express');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT `id`, `name` FROM `comments`', (error, results) => {
            if (error) {
                return res.status(500).send({error: error.sqlMessage});
            }

            res.send(results);
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `comments` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                return res.status(500).send({error: error.sqlMessage});
            }

            if (results[0]) {
                res.send(results[0]);
            } else {
                res.status(404).send({error: 'Comments not found'});
            }
        });
    });

    router.post('/', (req, res) => {
        const newComments = req.body;

        connection.query('INSERT INTO `comments` (`news_id`, `name`, `message`) VALUES (?,?,?)',
            [newComments.news, newComments.name, newComments.message],
            (error, results) => {
                if (error) {
                    return res.status(500).send({error: error.sqlMessage});
                }

                if(req.body.name === '') {
                    newComments.name = 'Anonymous'
                }

                res.send({...newComments, id: results.insertId});
            });
    });


    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `comments` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                return res.status(500).send({error: error.sqlMessage});
            }

            res.send({message: 'Comments deleted!'});
        });
    });


    return router;
};



module.exports = createRouter;
