import React, {Component, Fragment} from 'react';
import NewsForm from "../../containers/NewsForm/NewsForm";
import {connect} from "react-redux";
import {createNews} from "../../store/actions/newsActions";

class NewProduct extends Component {
    addNews = productData => {
        this.props.createNews(productData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New news</h2>
                <NewsForm onSubmit={this.addNews}/>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createNews: (news, history) => dispatch(createNews(news, history)),
});

export default connect(null, mapDispatchToProps)(NewProduct);