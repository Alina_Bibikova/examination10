import React from 'react';
import {Card, CardBody, CardText, CardTitle} from "reactstrap";
import NewsThumbnail from "../NewsThumbnail/NewsThumbnail";

const InfoNews = props => {
    return (
        <Card style={{marginBottom: '10px'}}>
            <NewsThumbnail image={props.image}/>
            <CardBody>
                <CardTitle >{props.title}</CardTitle>
                <CardText >{props.description}</CardText>
                <CardText >{props.datetime}</CardText>
            </CardBody>
        </Card>
    );
};

export default InfoNews;