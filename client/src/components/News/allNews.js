import React from 'react';
import NewsThumbnail from "../NewsThumbnail/NewsThumbnail";
import {Button, Card, CardBody, CardFooter, CardText, CardTitle} from "reactstrap";

const AllNews = props => {
    return (
        <Card style={{marginBottom: '10px'}}>
            <NewsThumbnail image={props.image}/>
            <CardBody>
            <CardTitle >{props.title}</CardTitle>
            <CardText >{props.description}</CardText>
            <CardText >{props.datetime}</CardText>
            </CardBody>
            <CardFooter>
                <Button onClick={props.deleteNews} style={{margin: '10px'}} color="danger">Delete</Button>
                <Button onClick={props.info} color="info">Info</Button>
            </CardFooter>
        </Card>
    );
};

export default AllNews;