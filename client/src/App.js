import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router";
import {NavLink as RouterNavLink} from "react-router-dom";

import News from "./containers/News/News";

import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import './App.css';
import NewNews from "./components/NewNews/NewNews";
import Comments from "./containers/Comments/Comments";

class App extends Component {
  render() {
    return (
      <Fragment>
          <Navbar color="light" light expand="md">
              <NavbarBrand tag={RouterNavLink} to="/" exact>All news</NavbarBrand>
              <NavbarToggler/>
              <Collapse isOpen navbar>
                  <Nav className="ml-auto" navbar>
                      <NavItem>
                          <NavLink tag={RouterNavLink} to="/" exact>News</NavLink>
                      </NavItem>
                  </Nav>
              </Collapse>
          </Navbar>
          <Container>
          <Switch>
              <Route path="/" exact component={News}/>
              <Route path="/news/add" exact component={NewNews}/>
              <Route path="/news/:id" exact component={Comments}/>
              <Route render={() => <h1>Hot found</h1>}/>
          </Switch>
          </Container>
      </Fragment>
    );
  }
}

export default App;
