import React, {Component, Fragment} from 'react';
import AllNews from '../../components/News/allNews';
import {deleteNews, fetchNews, infoComments} from "../../store/actions/newsActions";
import {connect} from "react-redux";
import {Button, Col} from "reactstrap";
import {Link} from "react-router-dom";

class News extends Component {

    componentDidMount() {
        this.props.fetchNews();
    }

    render() {
        return (
            <Fragment>
                <h2>Posts
                    <Link to="/news/add">
                        <Button style={{marginBottom: '10px'}}
                            color="primary"
                            className="float-right"
                        >
                            Add news
                        </Button>
                    </Link>
                </h2>
                <Col sm={10}>
                    {this.props.news.map(news => (
                        <AllNews
                            key={news.id}
                            title={news.title}
                            image={news.image}
                            datetime={news.datetime}
                            deleteNews={() => this.props.deleteNews(news.id)}
                            info={() => this.props.infoComments(news.id)}
                        />
                    ))}
                </Col>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        news: state.news.news,
        loading: state.loading,
        error: state.error,
    }};

const mapDispatchToProps = dispatch => ({
    fetchNews: () => dispatch(fetchNews()),
    deleteNews: id => dispatch(deleteNews(id)),
    infoComments: id => dispatch(infoComments(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(News);