import React, {Component} from 'react';

import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class NewsForm extends Component {
    state = {
        title: '',
        description: '',
        image: '',
    };

    submitFormHandler = event => {
        event.preventDefault();

        const news = {...this.state};
        news.datetime = new Date().toISOString();

        const formData = new FormData();
        Object.keys(news).forEach(key => {
            formData.append(key, news[key]);
        });

        this.props.onSubmit(formData);
        this.setState({
            title: '',
            description: '',
        })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler  = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormGroup row>
                    <Label sm={2} for="title">Title</Label>
                    <Col sm={10}>
                        <Input
                            type="text" required
                            name="title" id="title"
                            placeholder="Enter title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="description">Description</Label>
                    <Col sm={10}>
                        <Input
                            type="textarea" required
                            name="description" id="description"
                            placeholder="Enter description"
                            value={this.state.description}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="Image">Image</Label>
                    <Col sm={10}>
                        <Input
                            name="image"
                            type="file"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }

}

export default NewsForm;
