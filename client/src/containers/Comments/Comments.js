import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import InfoNews from "../../components/InfoNews/InfoNews";
import {connect} from "react-redux";
import {fetchComments} from "../../store/actions/commentsActions";

class Comments extends Component {

    state = {
        name: '',
        message: '',
    };

    componentDidMount() {
        this.props.fetchComments();
    }

    submitFormHandler = event => {
        event.preventDefault();

        const news = {...this.state};
        news.datetime = new Date().toISOString();

        const formData = new FormData();
        Object.keys(news).forEach(key => {
            formData.append(key, news[key]);
        });

        this.props.onSubmit(formData);
        this.setState({
            name: '',
            message: '',
        })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    render() {
        return (
            <Fragment>
                <InfoNews/>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="name">Name</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="name"
                                placeholder="Enter name"
                                value={this.state.name}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="message">Message</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea" required
                                name="message"
                                placeholder="Enter message"
                                value={this.state.message}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        news: state.news.comments,
        loading: state.loading,
        error: state.error,
    }};

const mapDispatchToProps = dispatch => ({
    fetchComments: () => dispatch(fetchComments()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);