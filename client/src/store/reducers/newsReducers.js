import {
    CREATE_NEWS_FAILURE,
    CREATE_NEWS_REQUEST,
    CREATE_NEWS_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    CREATE_COMMENTS_FAILURE,
    CREATE_COMMENTS_REQUEST,
    CREATE_COMMENTS_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS,
} from "../actions/actionsTypes";

const initialState = {
    news: [],
    comments: [],
    loading: true,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {
                ...state,
                loading: true
            };

        case FETCH_NEWS_SUCCESS:
            return {
                ...state,
                news: action.news,
                loading: false
            };

        case FETCH_NEWS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case CREATE_NEWS_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case CREATE_NEWS_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
            };

        case CREATE_NEWS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error.response.data.error
            };

        case FETCH_COMMENTS_REQUEST:
            return {
                ...state,
                loading: true
            };

        case FETCH_COMMENTS_SUCCESS:
            return {
                ...state,
                comments: action.comments,
                loading: false
            };

        case FETCH_COMMENTS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case CREATE_COMMENTS_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case CREATE_COMMENTS_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
            };

        case CREATE_COMMENTS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error.response.data.error
            };

        default:
            return state
    }
};

export default reducer;
