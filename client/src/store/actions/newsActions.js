import axios from '../../axios-api';

import {
    CREATE_NEWS_FAILURE,
    CREATE_NEWS_REQUEST,
    CREATE_NEWS_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS
} from "./actionsTypes";

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsFailure = error => ({type: FETCH_NEWS_FAILURE, error});

export const createNewsRequest = () => ({type: CREATE_NEWS_REQUEST});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});
export const createNewsFailure = error => ({type: CREATE_NEWS_FAILURE, error});

export const fetchNews = () => {
    return dispatch => {
        dispatch(fetchNewsRequest());
        axios.get('/news').then(response => {
            dispatch(fetchNewsSuccess(response.data))
        }).catch(error => dispatch(fetchNewsFailure(error)));
    };
};

export const infoComments = id => {
    return dispatch => {
       dispatch(fetchNewsRequest());
       return axios.get(`/news/${id}`).then(
          error => console.log(error)
       )
    };
};

export const createNews = (message) => {
    return dispatch => {
        dispatch(createNewsRequest());
        return axios.post('/news', message).then(
            () => dispatch(createNewsSuccess()),
            error => dispatch(createNewsFailure(error))
        )
    };
};

export const deleteNews = id => {
    return dispatch => {
        dispatch(fetchNewsRequest());
        return axios.delete(`/news/${id}`).then(
            error => console.log(error)
        )
    };
};

