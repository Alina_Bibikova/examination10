import axios from '../../axios-api';

import {
    CREATE_COMMENTS_FAILURE,
    CREATE_COMMENTS_REQUEST,
    CREATE_COMMENTS_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "./actionsTypes";

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = news => ({type: FETCH_COMMENTS_SUCCESS, news});
export const fetchNewsFailure = error => ({type: FETCH_COMMENTS_FAILURE, error});

export const createCommentsRequest = () => ({type: CREATE_COMMENTS_REQUEST});
export const createCommentsSuccess = () => ({type: CREATE_COMMENTS_SUCCESS});
export const createCommentsFailure = error => ({type: CREATE_COMMENTS_FAILURE, error});

export const fetchComments = () => {
    return dispatch => {
        dispatch(fetchCommentsRequest());
        axios.get('/news').then(response => {
            dispatch(fetchCommentsSuccess(response.data))
        }).catch(error => dispatch(fetchNewsFailure(error)));
    };
};

export const createComments = (message) => {
    return dispatch => {
        dispatch(createCommentsRequest());
        return axios.post('/news', message).then(
            () => dispatch(createCommentsSuccess()),
            error => dispatch(createCommentsFailure(error))
        )
    };
};


